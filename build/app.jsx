import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap';
import './main.scss';
import  Header from './layout/header.jsx';
import  Footer from './layout/footer.jsx';
import  Card from './components/card.jsx';


/**
* App.jsx: main entry file
* @since: v.1.0.0
*/
const App = () => {
  return(
  
<div className="container">
<Header />

{/* <div className="logo mb-4 d-flex justify-content-center">
<img src="https://b-static.karusel-tv.ru/images/logo.png?1e14fd1" alt=""/>
</div> */}

<div className="row justify-content-md-center">
<div className="col-md-10">
<h1>H1. Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя.</h1>
<p className="lead">Среди здешних <a href=""> величественных городов</a> и тёмных островов вам могут повстречаться искусный ниндзя Сенсей Ву и четверо его учеников — Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя. Их враг Лорд Гармадон обитает под землёй и жаждет обрести силу Золотого Оружия — меча Огня, косы Землятресений, нунчак Молнии и сюрикенов Льда. Если это оружие попадёт в его руки, зло Гармадоны вырвется из подземелья и расползётся по всему Ниндзяго!..</p>

<h2>H1. Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя.</h2>
<p className="lead">Среди здешних величественных городов и тёмных островов вам могут повстречаться искусный ниндзя Сенсей Ву и четверо его учеников — Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя. Их враг Лорд Гармадон обитает под землёй и жаждет обрести силу Золотого Оружия — меча Огня, косы Землятресений, нунчак Молнии и сюрикенов Льда. Если это оружие попадёт в его руки, зло Гармадоны вырвется из подземелья и расползётся по всему Ниндзяго!..</p>

<h3>H1. Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя.</h3>
<p className="lead">Среди здешних величественных городов и тёмных островов вам могут повстречаться искусный ниндзя Сенсей Ву и четверо его учеников — Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя. Их враг Лорд Гармадон обитает под землёй и жаждет обрести силу Золотого Оружия — меча Огня, косы Землятресений, нунчак Молнии и сюрикенов Льда. Если это оружие попадёт в его руки, зло Гармадоны вырвется из подземелья и расползётся по всему Ниндзяго!..</p>

<h4>H1. Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя.</h4>
<p className="lead">Среди здешних величественных городов и тёмных островов вам могут повстречаться искусный ниндзя Сенсей Ву и четверо его учеников — Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя. Их враг Лорд Гармадон обитает под землёй и жаждет обрести силу Золотого Оружия — меча Огня, косы Землятресений, нунчак Молнии и сюрикенов Льда. Если это оружие попадёт в его руки, зло Гармадоны вырвется из подземелья и расползётся по всему Ниндзяго!..</p>
<ul className="list-unstyled lead mb-4">
  <li>Lorem ipsum dolor sit amet</li>
  <li>Consectetur adipiscing elit</li>
  <li>Integer molestie lorem at massa</li>
  <li>Facilisis in pretium nisl aliquet</li>
  <li>Nulla volutpat aliquam velit
    <ul>
      <li>Phasellus iaculis neque</li>
      <li>Purus sodales ultricies</li>
      <li>Vestibulum laoreet porttitor sem</li>
      <li>Ac tristique libero volutpat at</li>
    </ul>
  </li>
  <li>Faucibus porta lacus fringilla vel</li>
  <li>Aenean sit amet erat nunc</li>
  <li>Eget porttitor lorem</li>
</ul>
<h5>H1. Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя.</h5>
<p className="lead">Среди здешних величественных городов и тёмных островов вам могут повстречаться искусный ниндзя Сенсей Ву и четверо его учеников — Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя. Их враг Лорд Гармадон обитает под землёй и жаждет обрести силу Золотого Оружия — меча Огня, косы Землятресений, нунчак Молнии и сюрикенов Льда. Если это оружие попадёт в его руки, зло Гармадоны вырвется из подземелья и расползётся по всему Ниндзяго!..</p>

<h6>H1. Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя.</h6>
<p className="lead">Среди здешних величественных городов и тёмных островов вам могут повстречаться искусный ниндзя Сенсей Ву и четверо его учеников — Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя. Их враг Лорд Гармадон обитает под землёй и жаждет обрести силу Золотого Оружия — меча Огня, косы Землятресений, нунчак Молнии и сюрикенов Льда. Если это оружие попадёт в его руки, зло Гармадоны вырвется из подземелья и расползётся по всему Ниндзяго!..</p>
<blockquote className="blockquote">
  <p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer className="blockquote-footer">Someasone famous in <cite title="Source Title">Source Title</cite></footer>
</blockquote>
<p className="lead">Среди здешних величественных городов и тёмных островов вам могут повстречаться искусный ниндзя Сенсей Ву и четверо его учеников — Джей, Кай, Зейн и Коул, которые постигают древнее искусство и готовятся стать настоящими ниндзя. Их враг Лорд Гармадон обитает под землёй и жаждет обрести силу Золотого Оружия — меча Огня, косы Землятресений, нунчак Молнии и сюрикенов Льда. Если это оружие попадёт в его руки, зло Гармадоны вырвется из подземелья и расползётся по всему Ниндзяго!..</p>

<h3>Inline elems</h3>
<p className="lead">You can use the mark tag to <mark>highlight</mark> text.</p>
<p className="lead"><del>This line of text is meant to be treated as deleted text.</del></p>
<p className="lead"><s>This line of text is meant to be treated as no longer accurate.</s></p>
<p className="lead"><ins>This line of text is meant to be treated as an addition to the document.</ins></p>
<p className="lead"><u>This line of text will render as underlined</u></p>
<p className="lead"><small>This line of text is meant to be treated as fine print.</small></p>
<p className="lead"><strong>This line rendered as bold text.</strong></p>
<p className="lead"><em>This line rendered as italicized text.</em></p>

<h3>Цитата</h3>

<blockquote className="blockquote">
  <p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer className="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
</blockquote>

<h3>Список</h3>
<ul class="list-unstyled">
  <li>Lorem ipsum dolor sit amet</li>
  <li>Consectetur adipiscing elit</li>
  <li>Integer molestie lorem at massa</li>
  <li>Facilisis in pretium nisl aliquet</li>
  <li>Nulla volutpat aliquam velit
    <ul>
      <li>Phasellus iaculis neque</li>
      <li>Purus sodales ultricies</li>
      <li>Vestibulum laoreet porttitor sem</li>
      <li>Ac tristique libero volutpat at</li>
    </ul>
  </li>
  <li>Faucibus porta lacus fringilla vel</li>
  <li>Aenean sit amet erat nunc</li>
  <li>Eget porttitor lorem</li>
</ul>

<section className="my-4">
<h3>Алерт</h3>
<div className="alert alert-primary" role="alert">
  A simple primary alert—check it out!
</div>
<div className="alert alert-secondary" role="alert">
  A simple secondary alert—check it out!
</div>
<div className="alert alert-success" role="alert">
  A simple success alert—check it out!
</div>
<div className="alert alert-danger" role="alert">
  A simple danger alert—check it out!
</div>
<div className="alert alert-warning" role="alert">
  A simple warning alert—check it out!
</div>
<div className="alert alert-info" role="alert">
  A simple info alert—check it out!
</div>
<div className="alert alert-light" role="alert">
  A simple light alert—check it out!
</div>
<div className="alert alert-dark" role="alert">
  A simple dark alert—check it out!
</div>
<div className="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Holy guacamole!</strong> You should check in on some of those fields below.
  <button type="button" className="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
</section>

<section className="my-4">
<div className="alert alert-success" role="alert">
  <h4 className="alert-heading">Well done!</h4>
  <p className="lead">Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
  <hr/>
  <p className="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
</div>
</section>


<section className="mb-4 mt-4">
<h3>Кнопки</h3>
<button type="button" className="btn btn-primary mr-1">Primary</button>
<button type="button" className="btn btn-secondary mr-1">Secondary</button>
<button type="button" className="btn btn-success mr-1">Success</button>
<button type="button" className="btn btn-danger mr-1">Danger</button>
<button type="button" className="btn btn-warning mr-1">Warning</button>
<button type="button" className="btn btn-info mr-1">Info</button>
<button type="button" className="btn btn-light mr-1">Light</button>
<button type="button" className="btn btn-dark mr-1">Dark</button>
<button type="button" className="btn btn-link mr-1">Link</button>
</section>

<section className="mb-4">
<h6>Outline</h6>
<button type="button" className="btn btn-outline-primary mr-1">Primary</button>
<button type="button" className="btn btn-outline-secondary mr-1">Secondary</button>
<button type="button" className="btn btn-outline-success mr-1">Success</button>
<button type="button" className="btn btn-outline-danger mr-1">Danger</button>
<button type="button" className="btn btn-outline-warning mr-1">Warning</button>
<button type="button" className="btn btn-outline-info mr-1">Info</button>
<button type="button" className="btn btn-outline-light mr-1">Light</button>
<button type="button" className="btn btn-outline-dark mr-1">Dark</button>
</section>

<section className="mb-4">
<button type="button" className="btn btn-primary btn-lg mr-1">Large button</button>
<button type="button" className="btn btn-secondary btn-lg mr-1">Large button</button>
</section>
<section className="mt-4">
  <h3>Формы</h3>
<form>
  <div className="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div className="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password"/>
  </div>
  <div className="form-group form-check">
    <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
    <label className="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <div className="form-group">
    <label for="exampleFormControlSelect1">Example select</label>
    <select className="form-control" id="exampleFormControlSelect1">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>
  <button type="submit" className="btn btn-primary">Submit</button>
</form> 


</section>
<section className="my-4">
<h3>Bages</h3>
    <span className="badge badge-primary mr-1">Primary</span>
    <span className="badge badge-secondary mr-1">Secondary</span>
    <span className="badge badge-success mr-1">Success</span>
    <span className="badge badge-danger mr-1">Danger</span>
    <span className="badge badge-warning mr-1">Warning</span>
    <span className="badge badge-info mr-1">Info</span>
    <span className="badge badge-light mr-1">Light</span>
    <span className="badge badge-dark mr-1">Dark</span>
</section>
<section className="my-4">
<button type="button" className="btn btn-primary">
  Notifications <span className="badge badge-light">4</span>
</button>
</section>



</div> 

</div>
<section className="my-4 px-3 py-3 bg-dark">
<h3 className="text-white">Карточка</h3>
<div className="row">
<div className="col-md-3">
<Card />
</div>
</div>

</section>
<Footer />
</div>
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('app')
);
